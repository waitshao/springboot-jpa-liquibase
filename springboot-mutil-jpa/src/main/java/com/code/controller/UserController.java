package com.code.controller;

import com.code.dao.primary.UserRepository;
import com.code.dao.second.PersonRepository;
import com.code.mode.primary.User;
import com.code.mode.second.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author liyufei
 * @since 2018-12-28 12:01
 */
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PersonRepository personRepository;

    @GetMapping("/user")
    public List<User> findAll() {
        userRepository.save(User.builder().name("xiaoqiang").phone("120").build());
        return userRepository.findAll();
    }

    @GetMapping("/person")
    public List<Person> findAllPerson() {
        personRepository.save(Person.builder().email("123").password("123").build());
        return personRepository.findAll();
    }
}
