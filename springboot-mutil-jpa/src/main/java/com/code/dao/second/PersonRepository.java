package com.code.dao.second;

import com.code.mode.second.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author liyufei
 * @since 2018-12-28 13:46
 */
public interface PersonRepository extends JpaRepository<Person, Integer> {
}
