package com.code.dao.primary;

import com.code.mode.primary.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author liyufei
 * @since 2018-12-28 13:45
 */
public interface UserRepository extends JpaRepository<User, Integer> {
}
