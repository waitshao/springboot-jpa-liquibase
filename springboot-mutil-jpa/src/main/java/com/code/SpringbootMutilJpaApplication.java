package com.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liyufei
 * @since 2018-12-28 11:56
 */
@SpringBootApplication
public class SpringbootMutilJpaApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootMutilJpaApplication.class, args);
    }
}
