package com.code.controller;

import com.code.dao.UserRepository;
import com.code.mode.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author liyufei
 * @since 2018-12-28 12:01
 */
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user")
    public List<User> findAll() {
        userRepository.save(User.builder().username("xiaoqiang").phone("120").build());
        return userRepository.findAll();
    }
}
