package com.code.dao;

import com.code.mode.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author liyufei
 * @since 2018-12-28 12:00
 */
public interface UserRepository extends JpaRepository<User, Integer> {
}
