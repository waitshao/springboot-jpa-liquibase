package com.code.dao.primary;

import com.code.mode.primary.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author liyufei
 * @since 2018-12-28 13:45
 */
public interface PersonRepository extends JpaRepository<Person, Integer> {
}
