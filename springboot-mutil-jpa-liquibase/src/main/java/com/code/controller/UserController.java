package com.code.controller;

import com.code.dao.primary.PersonRepository;
import com.code.dao.second.ProductRepository;
import com.code.mode.primary.Person;
import com.code.mode.second.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author liyufei
 * @since 2018-12-28 12:01
 */
@RestController
public class UserController {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/person")
    public List<Person> findAll() {
        personRepository.save(Person.builder().name("lisi").age(12).build());
        return personRepository.findAll();
    }

    @GetMapping("/product")
    public List<Product> findAllPerson() {
        productRepository.save(Product.builder().name("wangwu").price(12).build());
        return productRepository.findAll();
    }
}
