package com.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liyufei
 * @since 2018-12-28 18:24
 */
@SpringBootApplication
public class SpringBootMuitlApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootMuitlApplication.class, args);
    }
}
